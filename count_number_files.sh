#!/bin/sh

# grep -v "/.*/\|/../"
# grep "/./"    ==> match folder like ./1/xxxx or ./a/xxxxx     (only allow one char between / /)
# grep "/.*/"   ==> match folder linke ./1/xxx/ or ./111/xxxxx  (allow one or more chars between / /)

for folder in $(find . ! -path . -type d | grep -v "/.*/\|/../"); 
do
# echo -n option: no newline    
    echo -n $folder 
    echo -n -e '\t'
    echo $(find $folder -type f | wc -l)

done

# can use sort -t$'/t' -k2n result > result_sort to sort the result, based on second column
