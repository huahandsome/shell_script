#!/bin/sh

for folder in $(find . ! -path . -type d); do
    find $folder -type f -print -exec mv {} $1 \;
done
